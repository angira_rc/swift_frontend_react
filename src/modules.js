const axios = require('axios')

export async function request(url, method, data = null, headers = {}) {
    let resp
    let def = {
        "Content-Type": "application/json; charset=utf-8",
    }

    let request = {
        headers: {...def, ...headers},
        method: method,
        url: url,
        responseType:'json',
        data: JSON.stringify(data)
    }

    await axios(request).then(function (response) {
        resp = {
            data: response.data,
            status: response.status
        } 
    }).catch(function (error) {
        console.log(error)
        resp = {
            data: null,
            status: null
        }
    })
    
    return await resp
}

export async function check (server) {
    let req = await request(`${server}:5000/api`, 'GET')   
    
    if (req.status === 200) {
        return true
    } 

    return false
}