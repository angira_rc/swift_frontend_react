import { SET_URL } from '../actions'

function settings(state = { url: 'http://127.0.0.1' }, action) {
    switch (action.type) {
        case SET_URL:
            return {
                ...state,
                url: action.url
            }

        default:
            return state
    }
}

export default settings
