import { combineReducers } from 'redux'
import accounts from './accounts.js'
import settings from './settings.js'

const main = combineReducers({
  accounts,
  settings,
})

export default main
