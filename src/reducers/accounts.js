import { LOGIN, LOGOUT, UPDATE_HIERARCHY } from '../actions'

function accounts(state = { user: {}, token: null, hierarchy: null, history: [] }, action) {
    switch (action.type) {
        case LOGIN:
            return {
                ...state,
                loggedIn: true,
                user: action.attributes.user,
                token: action.attributes.token,
                hierarchy: action.attributes.hierarchy,
                history: action.attributes.history
            }

        case LOGOUT:
            return {
                ...state,
                loggedIn: false,
                user: null,
                token: null,
                hierarchy: null,
                history: null
            }

        case UPDATE_HIERARCHY:
            return {
                ...state,
                hierarchy: action.hierarchy
            }

        default:
            return state
    }
}

export default accounts
