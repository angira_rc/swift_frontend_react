export const LOGIN = 'LOGIN'
export const LOGOUT = 'LOGOUT'
export const SET_URL = 'SET_URL'
export const CHANGE_MAIN = 'CHANGE_MAIN'
export const UPDATE_HIERARCHY = 'UPDATE_HIERARCHY'

export function login (attributes) {
    const action = {
        type: LOGIN,
        attributes: attributes
    }
    return action
}

export function logout () {
    const action = {
        type: LOGOUT
    }
    return action
}

export function setUrl (url) {
    const action = {
      type: SET_URL,
      urls: url
    }
    return action
}

export function updateHierarchy (hierarchy) {
    const action = {
        type: UPDATE_HIERARCHY,
        hierarchy: hierarchy
      }
      return action
}