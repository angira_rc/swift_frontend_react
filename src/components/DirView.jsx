import React from 'react'
import { connect } from 'react-redux'

class DirView extends React.Component {
    render() {
        return (
            <h1>DirView</h1>
        )
    }
}

function stateToProps (state) {
    return {
        hierarchy: state.accounts.hierarchy
    }
}

export default connect(stateToProps, {})(DirView)