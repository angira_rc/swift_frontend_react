import React, { Component} from 'react'
import { Link } from 'react-router-dom'

class Home extends Component {
    render () {
        return (
            <div className="content">
                <div className="around">
                    <img className="logo" alt="logo" src="/res/images/logo.svg"/>
                    <h1>Swift</h1>
                    <br/>
                    <div className="accounts-buttons">
                        <Link to="/files">Log In</Link>
                        <Link to="/signup">Sign Up</Link>
                    </div>
                </div>
            </div>
        )
    }
}

export default Home