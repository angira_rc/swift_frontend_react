import React from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'

import { logout } from '../actions'

class Menu extends React.Component {
    render () {
        const routes = [
            ['/files', '<i class="fas fa-home"></i>', "dashboard"],
            ['/files/manager', '<i class="far fa-folder"></i>', "files"]
        ]

        return (
            <div className="menu">
                <div className="head">
                    <img className="logo" alt="logo" src="/res/images/logo.svg"/>
                </div>
                <div className="routes">
                    {routes.map((item, key) => {
                        let active = "icon"
                        if (item[2] === this.props.view) {
                            active="active"
                        }

                        return (
                            <Link 
                                to={ item[0] } 
                                key={ key }
                                index = { item[2] } 
                                className={ active } 
                                dangerouslySetInnerHTML={{ __html: item[1] }} 
                                onClick = { (e) => this.props.changeView(e.target.parentNode.getAttribute('index')) }></Link>
                        )
                    })}
                    <button className="no-settings" onClick={ this.props.openSettings }><i className="fas fa-cog"></i></button>
                </div>
                <div className="profile-icon">
                    <i className="far fa-user"></i>
                    <p>{ this.props.fname }</p>
                    <button className="logout" onClick={ () => this.props.logout() }>LOG OUT</button>
                </div>
            </div>
        )
    }
}

function stateToProps (state) {
    return {
        'fname': state.accounts.user.fname
    }
}

export default connect(stateToProps, { logout })(Menu)