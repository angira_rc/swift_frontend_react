import React from 'react'
import { connect } from 'react-redux'
import alertify from 'alertifyjs'
import 'react-quill/dist/quill.snow.css'

import { updateHierarchy } from '../actions'
import { request, check } from '../modules'

class Files extends React.Component {
    constructor () {
        super()
        this.state = {
            parent: null,
            selected: {
                type: null,
                name: null,
                path: null,
            },
            open: false
        }
        this.select = this.select.bind(this)
    }
    
    componentWillMount () {        
        this.setState({
            root: `users/${this.props.user.uname}/`,
            current: this.props.hierarchy,
            history: []
        })
    }

    select (e) {
        let selected = {
            type: e.target.getAttribute("data-type"),
            name: e.target.getAttribute("data-name"),
            path: e.target.getAttribute("data-path"),
        }
        
        this.setState({
            selected: selected
        })        
    }

    newFile () {
        let token = this.props.token
        let root = this.state.root
        let url = this.props.url

        let update = (data) =>  this.props.updateHierarchy(data)
        
        alertify.prompt('Create New File', 'File name', async function(evt, value){ 
            let authHeader = {
                "Authorization": "Basic " + btoa(`${token}:unused}`),
            }
    
            let data = {
                type: "file",
                path: root,
                name: value
            }
            
            if (await check(url)) {  
                let req = await request(`${url}:5000/api/files`, 'POST', data, authHeader)
                
                if (req.status === 200) {
                    if (req.data.OK) {
                        alertify.success("Success")
                        update(req.data.data.hierarchy)

                    } else {
                        alertify.error(req.data.data.error)
                    }
                } else {
                    alertify.error("A problem was encountered")
                }
            } else {
                alertify.error("Our servers are offline.")
            }
        });
    }
    
    async write () {
        let authHeader = {
            "Authorization": "Basic " + btoa(`${this.props.token}:unused}`),
        }
        let data = {
            op: "read",
            path: this.state.root,
            name: this.state.selected.name
        }

        if (await check(this.props.url)) {  
            let req = await request(`${this.props.url}:5000/api/files/read`, 'POST', data, authHeader)

            if (req.status === 200) {
                this.props.openEditor({
                    name: data.name,
                    content: req.data.data.contents
                })
            } else {
                alertify.error("File doesn't exist!")
            }
        } else {
            alertify.error("Our servers are offline!")
        }
    }

    rename () {
        if (this.state.selected.name) {
            let token = this.props.token
            let root = this.state.root
            let url = this.props.url
            let name = this.state.selected.name

            let update = (data) =>  this.props.updateHierarchy(data)

            alertify.prompt('Rename file', name, async function(evt, value){ 
                let authHeader = {
                    "Authorization": "Basic " + btoa(`${token}:unused}`),
                }
        
                let data = {
                    type: "file",
                    operation: "rename",
                    path: root,
                    name: name,
                    new: value
                }

                if (await check(url)) {          
                    let req = await request(`${url}:5000/api/files`, 'PUT', data, authHeader)
                    
                    if (req.status === 200) {
                        console.log(req)
                        if (req.data.OK) {
                            alertify.success("Success")
                            update(req.data.data.hierarchy)

                        } else {
                            alertify.error(req.data.data.error)
                        }
                    } else {
                        alertify.error("A problem was encountered")
                    }
                } else {
                    alertify.error("Our servers are offline!")
                }
            });
        } else {
            alertify.error("No file is selected")
        }
    }

    del () {
        if (this.state.selected.name) {
            let token = this.props.token
            let root = this.state.root
            let url = this.props.url
            let name = this.state.selected.name

            let update = (data) =>  this.props.updateHierarchy(data)
            alertify.confirm('Delete this file?', async function(){ 
                let authHeader = {
                    "Authorization": "Basic " + btoa(`${token}:unused}`),
                }
        
                let data = {
                    type: "file",
                    path: root,
                    name: name
                }

                if (await check(url)) {          
                    let req = await request(`${url}:5000/api/files`, 'DELETE', data, authHeader)
                    
                    if (req.status === 200) {
                        if (req.data.OK) {
                            alertify.success("Success")
                            update(req.data.data.hierarchy)
            
                        } else {
                            alertify.error(req.data.data.error)
                        }
                    } else {
                        alertify.error("A problem was encountered")
                    }
                } else {
                    alertify.error("Our servers are offline!")
                }
            })
        } else {
            alertify.error("Nothing is selected")
        }
    }

    changeDir () {
        return null
    }

    refresh (item) {
        if (item === 'hierarchy') {

        } else if (item === 'history') {

        } else if (item === 'user-info') {

        }
    }

    render () {
        return (
            <div className="files-view">
                <div className="file-manager">
                    <div className="actionBar">
                        <button onClick={ () => this.newFile() } className="new-file"><i className="fas fa-file"></i> New File</button>
                        <button onClick={ () => this.rename() } className="rename"><i className="fas fa-pencil-alt"></i>Rename</button>
                        <button onClick={ () => this.del() } className="delete"><i className="far fa-trash-alt"></i> Delete</button>
                    </div>
                    <div className="dirview">
                        {
                            this.props.hierarchy.children.map((item, key) => {
                                item = JSON.parse(item)
                                
                                let active = ""

                                if (this.state.selected.name === item.name) {
                                    active = " selected"
                                }

                                let icon = '<i class="fas fa-file"></i>'
                                                        
                                if (item.type === 'dir') {
                                    icon = '<i class="far fa-folder-open"></i>'
                                }

                                return (
                                    <div 
                                        key = { key } 
                                        className= { "file-manager-item" + active } 
                                        data-type = { item.type }
                                        data-name = { item.name }
                                        data-size = { item.size }
                                        onClick={ (e) => this.select(e) }
                                        onDoubleClick = { (e) => this.write() }>
                                        <div className="icon" dangerouslySetInnerHTML={{ __html: icon }}></div>
                                        <p>{ item.name }</p>
                                    </div>
                                )
                            }
                        )}
                    </div>
                </div>
            </div>
        )
    }
}

function stateToProps (state) {
    return {
        hierarchy: JSON.parse(state.accounts.hierarchy),
        user: state.accounts.user,
        token: state.accounts.token,
        url: state.settings.url
    }
}

export default connect(stateToProps, { updateHierarchy })(Files)