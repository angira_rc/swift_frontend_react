import Login from './Login'
import FileManager from './FileManager'
import React, { Component } from 'react'
import { connect } from 'react-redux'

class View extends Component {
    render() {
        if (this.props.loggedIn) {
            return (
                <FileManager />
            )
        } else {
            return (
                <Login />
            )
        }
    }
}

function mapStatetoProps(state) {
    return {
        loggedIn: state.accounts.loggedIn
    }
}

export default connect(mapStatetoProps, null)(View)