import React, { Component} from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import alertify from 'alertifyjs'

import { request } from '../modules'

class SignUp extends Component {
    constructor () {
        super()
        this.signup = this.signup.bind(this)
    }

    async signup () {
        let data = this.state

        let req = await request(`${this.props.url}:5000/api/user`, 'POST', data)   

        if (req.status === 201) {
            alertify.success(`User ${req.data.username} created successfully!`)
        } else {
            alertify.error('A problem was encountered')
        }        
    }

    render () {
        return (
            <div className="content">
                <div className="around">
                    <img className="logo" alt="logo" src="/res/images/logo.svg"/>
                    <h2>Sign Up</h2>
                    <div className="signup">
                        <div className="row form-row">
                            <div className="form-group has-success">
                                <label className="bmd-label-floating">First Name</label>
                                <input onBlur = { (e) => this.setState({ fName: e.target.value }) } type="text" className="form-control" id="fname"/>
                            </div>
                            <div className="form-group has-success">
                                <label className="bmd-label-floating">Last Name</label>
                                <input onBlur = { (e) => this.setState({ lName: e.target.value }) } type="text" className="form-control" id="lname"/>
                            </div>
                        </div>
                        <div className="form-group has-success">
                            <label className="bmd-label-floating">Email</label>
                            <input onBlur = { (e) => this.setState({ email: e.target.value }) } type="email" className="form-control" id="email"/>
                        </div>
                        <div className="row form-row">
                            <div className="form-group has-success">
                                <label className="bmd-label-floating">Username</label>
                                <input onBlur = { (e) => this.setState({ uName: e.target.value }) } type="text" className="form-control" id="uname"/>
                            </div>
                            <div className="form-group has-success">
                                <label className="bmd-label-floating">Phone Numer</label>
                                <input onBlur = { (e) => this.setState({ phone: e.target.value }) } type="text" className="form-control" id="phone"/>
                            </div>
                        </div>
                        <div className="form-group has-success">
                            <label className="bmd-label-floating">Password</label>
                            <input onBlur = { (e) => this.setState({ pwd: e.target.value }) } type="password" className="form-control" id="password"/>
                        </div>
                        <br/>
                        <button onClick = { this.signup } data-what="signup" className="btn btn-success">Sign Up</button>
                        <br/><br/>
                        <Link to="/" component="button">Back</Link>
                        <br/><br/><br/><br/>
                    </div>
                </div>
            </div>
        )
    }
}

function stateToProps (state) {
    return {
        url: state.settings.url
    }
}

export default connect(stateToProps, {})(SignUp)