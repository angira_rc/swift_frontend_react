import React, { Component} from 'react'
import alertify from 'alertifyjs'

import { login, setUrl } from '../actions'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { request, check } from '../modules'

class Login extends Component {
    constructor () {
        super()
        this.state = {
            uname: null,
            pwd: null,
            status: {
                ago: false,
                failed: false,
                msg: null
            }
        }
    }

    async fetchData (token) {        
        let authHeader = {
            "Authorization": "Basic " + btoa(`${token}:unused}`),
        }

        let attributes = {
            token: token
        }

        let req = await request(`${this.props.url}:5000/api/user/get`, 'GET', null, authHeader)
        
        if (req.status === 200) {
            let data = req.data.data
            
            attributes = {
                ...attributes,
                user: {
                    uname: data.uname,
                    fname: data.fname,
                    lname: data.lname,
                    email: data.email,
                    phone: data.phone
                }
            }

            req = await request(`${this.props.url}:5000/api/files/history`, 'GET', null, authHeader)
        
            if(req.status === 200) {
                attributes = {
                    ...attributes,
                    history: req.data.data
                }

                req = await request(`${this.props.url}:5000/api/files/hierarchy`, 'GET', null, authHeader)
                
                if(req.status === 200) {
                    attributes = {
                        ...attributes,
                        hierarchy: req.data.data
                    }

                    this.props.login(attributes)
                }
            }
        }
    }

    async login () {
        let data = {
            uname: this.state.uname,
            pwd: this.state.pwd
        }
        
        if (await check(this.props.url)) {  
            let req = await request(`${this.props.url}:5000/api/login`, 'POST', data)   

            if (req.status === 200) {
                this.setState({
                    status: {
                        ago: true,
                        failed: false,
                        msg: "Success!"
                    }
                })            

                this.fetchData(req.data.data)
                alertify.success('Successful')

            } else if (req.status === 404) {
                this.setState({
                    status: {
                        ago: false,
                        failed: true,
                        msg: "Account not found."
                    }
                })     
                alertify.error("Account not found.")       

            } else {
                this.setState({
                    status: {
                        ago: false,
                        failed: true,
                        msg: "A problem was encountered."
                    }
                })
                alertify.error("A problem was encountered.")
            }
        } else {
            alertify.error("Our servers are offline")
        }
    }
    render () {
        return (
            <div className="content">
                <div className="around">
                    <img className="logo" alt="logo" src="/res/images/logo.svg"/>
                    <br/>
                    <h2>Login</h2>
                    <div className="login">
                        <div className="form-group has-success">
                            <label className="bmd-label-floating">Username</label>
                            <input 
                                type="text" 
                                className="form-control" 
                                onBlur = { event => this.setState({ uname: event.target.value }) }
                            />
                        </div>
                        <div className="form-group has-success">
                            <label className="bmd-label-floating">Password</label>
                            <input 
                                type="password" 
                                className="form-control" 
                                onBlur = { event => this.setState({ pwd: event.target.value }) }
                            />
                        </div>
                        <br/>
                        <button
                            data-what="login" 
                            className="btn btn-success"
                            onClick = { () => this.login() }
                        >Log in</button>
                        <br/><br/>
                        <Link to="/" component="button">Back</Link>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStatetoProps (state) {
    return {
        url: state.settings.url,
        servers: state.settings.servers
    }
}

export default connect(mapStatetoProps, { login, setUrl })(Login)