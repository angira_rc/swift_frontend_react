import React from 'react'
import { connect } from 'react-redux'

class Profile extends React.Component {
    render () {
        return (
            <div className="files-view">
                <div className="card">
                    <div className="card-header card-header-warning">
                        <h4 className="card-title">User Profile</h4>
                        <p className="card-category">{this.props.user.name}</p>
                    </div>
                    <div className="card-body">
                        <form>
                            <div className="form-group">
                                <label className="bmd-label-floating">Username</label>
                                <input type="text" className="form-control" disabled value = { this.props.user.uname }/>
                            </div>
                            <div className="form-group">
                                <label className="bmd-label-floating">First Name</label>
                                <input type="text" className="form-control" value = { this.props.user.fname }/>
                            </div>
                            <div className="form-group">
                                <label className="bmd-label-floating">Last Name</label>
                                <input type="text" className="form-control" value = { this.props.user.lname }/>
                            </div>
                            <div className="form-group">
                                <label className="bmd-label-floating">Email address</label>
                                <input type="email" className="form-control" value = { this.props.user.email }/>
                            </div>
                            <button type="submit" className="btn btn-primary pull-right">Update Profile</button>
                            <div className="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

function stateToProps (state) {
    return {
        user: state.accounts.user
    }
}

export default connect(stateToProps, {})(Profile)