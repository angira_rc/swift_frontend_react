import React, { Component } from 'react'
import { connect } from 'react-redux'

class Settings extends Component {
    render () {
        return (
            <div className="files-view">
                <h1>Settings</h1>
                <p>{ this.props.url }</p>
            </div>
        )
    }
}

function stateToProps (state) {
    return {
        url: state.settings.url
    }
}

export default connect(stateToProps, {})(Settings)