import React, { Component} from 'react'
import { connect } from 'react-redux'
import { Switch, Route } from 'react-router'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import Button from '@material-ui/core/Button'
import DialogTitle from '@material-ui/core/DialogTitle'
import ReactQuill from 'react-quill'
import alertify from 'alertifyjs'

import { request, check } from '../modules'
import { setUrl } from '../actions'
import Menu from './Menu'
import Dashboard from './Dashboard'
import Profile from './Profile'
import Files from './Files'
import Settings from './Settings'

class FileManager extends Component {
    constructor () {
        super()
        this.state = {
            current: 'dashboard',
            open: false,
            settingsOpen: false,
            editorContent: {
                name: null,
                content: null
            },
            newUrl: null
        }
    }

    componentWillMount () {        
        this.setState({
            root: `users/${this.props.uname}/`
        })
    }

    async write () {
        let authHeader = {
            "Authorization": "Basic " + btoa(`${this.props.token}:unused}`),
        }

        let data = {
            type: "file",
            operation: "modify",
            path: this.state.root,
            name: this.state.editorContent.name,
            contents: this.state.editorContent.content
        }

        if (check(this.props.settings.url)) {
            let req = await request(`${this.props.settings.url}:5000/api/files`, 'PUT', data, authHeader)
        
            if (req.status === 200) {
                alertify.success("Updated successfully!")
            } else {
                alertify.error("File doesn't exist!")
            }
        } else {
            alertify.error("Our servers are offline!")
        }
    }

    render () {
        let val = this.state.editorContent.content
        let formats = [
            'video', 'image', 'link', 'blockquote',
            'bold', 'italic', 'underline', 'list', 
            'strike', 'header'
        ]

        let modules = {
            toolbar: {
                container: [
                    ['bold', 'italic', 'underline', 'strike'],
                    [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
                    [{ 'font': [] }],
                    [{ 'align': [] }],
                    [{ 'color': [] }, { 'background': [] }],                
                    [{ 'list': 'ordered'}, { 'list': 'bullet' }],
                    [{ 'script': 'sub'}, { 'script': 'super' }],
                    [{ 'indent': '-1'}, { 'indent': '+1' }],
                    [{ 'direction': 'rtl' }],
                    ['blockquote', 'image'],
                    ['link', 'video'],
                    ['clean'] 
                ]   
            }
       }
       
        return (
            <div className="content">
                <Dialog
                    fullWidth
                    maxWidth='md'
                    open={ this.state.settingsOpen }
                    onClose={ () => this.setState({ open: false }) }
                    aria-labelledby="max-width-dialog-title"
                    >
                    <DialogTitle id="max-width-dialog-title">Settings</DialogTitle>
                    <DialogContent>
                        <h2>Server IP Address</h2>                        
                        <br/>
                        <div className="container servers">
                            <div className="row">
                                <div className="col-md-8">
                                    <input 
                                        value={ this.props.settings.url } 
                                        className="server form-control" 
                                        onChange={ (e) => {
                                            this.setState({newUrl: e.target.value})
                                        } }/> 
                                </div>
                                <div className="col-md-4">
                                    <Button onClick={ () => this.props.setUrl(this.state.newUrl) } color="primary">Save</Button>
                                </div>
                            </div>
                        </div>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={ () => { this.setState({ settingsOpen: false }) } } color="primary">Close</Button>
                    </DialogActions>
                </Dialog>
                <Dialog
                    fullWidth
                    maxWidth='lg'
                    open={ this.state.open }
                    onClose={ () => this.setState({ open: false }) }
                    aria-labelledby="max-width-dialog-title"
                    >
                    <DialogTitle id="max-width-dialog-title">Edit { this.state.editorContent.name }</DialogTitle>
                    <DialogContent>
                        <ReactQuill formats = { formats } value={ val } onChange={ (value) => this.setState({ editorContent: { name: this.state.editorContent.name, content: value } }) } modules={ modules } />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={ () => this.setState({ open: false }) } color="default">
                            Cancel
                        </Button>
                        <Button 
                            onClick={ () => { 
                                this.setState({ open: false }) 
                                this.write()
                            } } color="primary">
                            Save
                        </Button>
                    </DialogActions>
                </Dialog>
                <div className="window">
                    <Menu view={ this.state.current } openSettings={ () => this.setState({settingsOpen: true}) } changeView={ (index) => this.setState({current: index}) }/>
                    <div className="view">
                        <Title view={ this.state.current } />
                        <br/>
                        <Switch>
                            <Route path="/profile" component={ Profile } />
                            <Route exact path="/files" component={ Dashboard } />
                            <Route 
                                path="/files/manager" 
                                component={ () => <Files openEditor = { 
                                    (current) => this.setState({
                                        open: true,
                                        editorContent: {
                                            name: current.name,
                                            content: current.content
                                        }
                                    }) } /> 
                                } />
                            <Route path="/settings" component={ Settings } />
                        </Switch>
                    </div>
                </div>
            </div>
        )
    }
}

function stateToProps (state) {
    return {
        uname: state.accounts.user.uname,
        token: state.accounts.token,
        settings: state.settings
    }
}

export default connect(stateToProps, { setUrl })(FileManager)

class Title extends Component {
    render () {
        let title = ""  
        
        switch (this.props.view) {
            case 'dashboard':
                title = "Dashboard"
                break;
            case 'files':
                title = "Files"
                break;
            case 'settings':
                title = "Settings"
                break;
            case 'profile':
                title = "Profile"
                break;
            default:
                title = "Dashboard"
                break;
        }
        
        return (
            <div className="title">
                <h1>{ title }</h1>
            </div>
        )
    }
}