import React from 'react'
import { connect } from 'react-redux'

class Dashboard extends React.Component {
    render() {
        return (
            <div className="files-view">
                <div className="dash">
                    <div className="card">
                        <div className="card-header card-header-warning">
                            <h4 className="card-title">File History</h4>
                            <p className="card-category">{this.props.name}</p>
                        </div>
                        <div className="card-body table-responsive">
                            <table className="table table-hover">
                                <thead className="text-warning">
                                    <tr><th>No.</th>
                                        <th>Type</th>
                                        <th>Description</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.props.history.map((item, i) => {
                                        return (
                                            <tr key={i}>
                                                <td>{i + 1}</td>
                                                <td>{item.typeOf}</td>
                                                <td>{item.name}</td>
                                            </tr>
                                        )
                                    })}

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        history: state.accounts.history,
        name: state.accounts.user.fname + " " + state.accounts.user.lname
    }
}

export default connect(mapStateToProps, {})(Dashboard)