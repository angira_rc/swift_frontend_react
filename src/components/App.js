import React, { Component } from 'react'
import { Switch, Route } from 'react-router'
import Home from './Home'
import View from './View'
import SignUp from './SignUp'
import './App.css';

class App extends Component {
    render() {
        return (
            <div className="App">
                <Switch>
                    <Route exact path="/" component={ Home } />
                    <Route path="/files" component={ View } />
                    <Route path="/signup" component={ SignUp } />
                </Switch>
            </div>
        );
    }
}

export default App;
