import React from 'react'
import ReactDOM from 'react-dom'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import App from './components/App'
import * as serviceWorker from './serviceWorker'
import { BrowserRouter as Router } from "react-router-dom"
import main from './reducers'

const store = createStore(main)
store.subscribe(() => console.log(store.getState()))

ReactDOM.render(
    <Provider store = { store }>
        <Router>
            <App />
        </Router>
    </Provider>, 
    document.getElementById('root'));

    serviceWorker.unregister();
